# Pulling Image


## Latest Image
```
apptainer pull oras://gitlab-registry.oit.duke.edu/granek-lab/granek-container-images/alspaugh-rim101-rra1-simg:latest
```

## Published Version
```
apptainer pull oras://gitlab-registry.oit.duke.edu/granek-lab/granek-container-images/alspaugh-rim101-rra1-simg:v001
```
